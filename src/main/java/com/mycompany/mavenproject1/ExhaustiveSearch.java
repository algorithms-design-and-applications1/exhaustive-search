/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class ExhaustiveSearch {

    public int maxProfit(int[] prices) {
        int minPriceSoFar = Integer.MAX_VALUE, maxProfitSoFar = 0;

        for (int i = 0; i < prices.length; i++) {
            if (minPriceSoFar > prices[i]) {
                minPriceSoFar = prices[i];
            } else {
                maxProfitSoFar = Math.max(maxProfitSoFar, prices[i] - minPriceSoFar);
            }
        }

        return maxProfitSoFar;

    }

    
        
}
